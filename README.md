Sample no UI cms for articles with API methods for authorized CRUD and un-authorized view
===========================

Installation:
- run composer install
- configure .env.local with correct access to database
- configure .env.test.local with separate DB for testing
- run bin/console doctrine:migrations:migrate
- php bin/console --env=test doctrine:migrations:migrate
- symfony server:start for running a webserver (symfony cli is required)


##Admin CRUD:

accessible if request contains X-Secure-Access with value 'very_secret'


POST /admin/articles/ - create new article

- title (string, 255 max) 
- body (string, no limit)

POST /admin/articles/{id} - update existing article

- title (string, 255 max)
- body (string, no limit)

DELETE /admin/articles/{id} - delete article


POST /admin/articles/list - get a list of all articles (POST used instead of GET for unification, since GET can't have body and will require usage of url parameters)

- sortField (id\title or no value)
- sortDest (ASC\DESC  or no value)
- limit (int  or no value)
- offset (int or no value)

POST /admin/articles/list/{id} - view one article 

##API:

POST /api/articles/ - get a list of all articles (POST used instead of GET for unification, since GET can't have body and will require usage of url parameters)

- sortField (id\title or no value)
- sortDest (ASC\DESC  or no value)
- limit (int  or no value)
- offset (int or no value)

POST /api/articles/{id} - view one article 


