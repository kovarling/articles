<?php

declare(strict_types=1);


namespace App\Media\Blog\Domain;


use App\Shared\Domain\FieldsFormatter;

class ArticleApiFields implements FieldsFormatter
{

    public static function getFieldsList() : array
    {
        return ['id', 'title', 'createdAt'];
    }

    public static function getFieldsView() : array
    {
        return ['id', 'title', 'body', 'createdAt'];
    }

}