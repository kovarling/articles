<?php


namespace App\Media\Blog\Domain;

use Doctrine\ORM\EntityNotFoundException;

/**
 * Interface ArticleRepository
 * @package App\Media\Blog\Domain
 */
interface ArticleRepository
{

    /**
     * @param int $id
     * @throws EntityNotFoundException
     */
    public function delete(int $id) : void;

    /**
     * @param Article $article
     */
    public function create(Article $article) : Article;

    /**
     * @param Article $article
     */
    public function update(Article $article) : Article;

    /**
     * @param array $fields
     * @param ?string $sortField
     * @param ?string $sortDest
     * @param ?int $limit
     * @param ?int $offset
     * @return array // TODO: create proxy classes
     */
    public function getList(array $fields, ?string $sortField, ?string $sortDest, ?int $limit, ?int $offset) : array;

    /**
     * @param int $id
     * @return Article
     * @throws EntityNotFoundException
     */
    public function findArticle(int $id) : Article;

    /**
     * @param int $id
     * @param array $fields
     * @return array // TODO: create proxy classes
     * @throws EntityNotFoundException
     */
    public function viewArticle(int $id, array $fields) : array;

}