<?php

declare(strict_types=1);


namespace App\Media\Blog\Domain;


class ArticleSorting
{

    private ?string $sortField = null;
    private ?string $sortDest = null;
    private ?int $limit = null;
    private ?int $offset = null;

    /**
     * @return string|null
     */
    public function getSortField(): ?string
    {
        return $this->sortField;
    }

    /**
     * @param string|null $sortField
     */
    public function setSortField(?string $sortField): void
    {
        $this->sortField = $sortField;
    }

    /**
     * @return string|null
     */
    public function getSortDest(): ?string
    {
        return $this->sortDest;
    }

    /**
     * @param string|null $sortDest
     */
    public function setSortDest(?string $sortDest): void
    {
        $this->sortDest = $sortDest;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * @param int|null $offset
     */
    public function setOffset(?int $offset): void
    {
        $this->offset = $offset;
    }


}