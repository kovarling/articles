<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Admin;


use App\Media\Blog\Domain\ArticleRepository;
use App\Shared\Infrastructure\ApiAction;
use App\Shared\Traits\FormJsonTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

abstract class ArticleAction extends ApiAction
{
    use FormJsonTrait;

    protected MessageBusInterface $bus;
    protected ArticleRepository $repository;

    public function __construct(
        MessageBusInterface $bus,
        DecoderInterface $decoder,
        ArticleRepository $repository
    )
    {
        $this->bus = $bus;
        $this->decoder = $decoder;
        $this->repository = $repository;
    }

}