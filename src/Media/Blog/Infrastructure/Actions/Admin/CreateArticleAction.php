<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Admin;


use App\Media\Blog\Infrastructure\Forms\ArticleType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class CreateArticleAction extends ArticleAction
{

    public function __invoke(Request $request) : JsonResponse
    {

        $form = $this->formFromJson($request, ArticleType::class);
        if($form->isValid()) {
            $article = $this->repository->create($form->getData());
            return $this->json($article);
        } else {
            return $this->errorForm($form);
        }

    }

}