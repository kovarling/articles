<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Admin;


use App\Media\Blog\Infrastructure\Message\DeleteArticleMessage;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class DeleteArticleAction extends ArticleAction
{

    public function __invoke(int $id) : JsonResponse
    {
        try{
            $article = $this->repository->findArticle($id);
        } catch (EntityNotFoundException $e) {
            return $this->errorMessage('Article not found', Response::HTTP_NOT_FOUND);
        }

        // there might be more complex logic for deletion of an article
        $this->bus->dispatch(new DeleteArticleMessage($article->getId()));

        return $this->successResult('Article queued for deletion');

    }

}