<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Admin;

use App\Media\Blog\Domain\ArticleAdminFields;
use App\Media\Blog\Domain\ArticleSorting;
use App\Media\Blog\Infrastructure\Forms\ArticleListType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListArticleAction extends ArticleAction
{

    public function __invoke(Request $request) : JsonResponse
    {
        $form = $this->formFromJson($request, ArticleListType::class);

        if($form->isValid()) {
            /** @var ArticleSorting $articleSorting */
            $articleSorting = $form->getData();

            $list = $this->repository->getList(
                ArticleAdminFields::getFieldsList(),
                $articleSorting->getSortField(),
                $articleSorting->getSortDest(),
                $articleSorting->getLimit(),
                $articleSorting->getOffset()
            );

            return $this->json(compact('list'));
        } else {
            return $this->errorForm($form);
        }
    }

}