<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Admin;


use App\Media\Blog\Infrastructure\Forms\ArticleType;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UpdateArticleAction extends ArticleAction
{

    public function __invoke(int $id, Request $request) : JsonResponse
    {
        try{
            $article = $this->repository->findArticle($id);
        } catch (EntityNotFoundException $e) {
            return $this->errorMessage('Article not found', Response::HTTP_NOT_FOUND);
        }

        $form = $this->formFromJson($request, ArticleType::class, $article);
        if($form->isValid()) {
            $article = $this->repository->update($form->getData());
            return $this->json($article);
        } else {
            return $this->errorForm($form);
        }

    }

}