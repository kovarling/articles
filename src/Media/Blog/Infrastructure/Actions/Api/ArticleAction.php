<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Api;


use App\Media\Blog\Domain\ArticleRepository;
use App\Shared\Infrastructure\ApiAction;
use App\Shared\Traits\FormJsonTrait;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

abstract class ArticleAction extends ApiAction
{
    use FormJsonTrait;

    protected ArticleRepository $repository;

    public function __construct(
        DecoderInterface $decoder,
        ArticleRepository $repository
    )
    {
        $this->decoder = $decoder;
        $this->repository = $repository;
    }

}