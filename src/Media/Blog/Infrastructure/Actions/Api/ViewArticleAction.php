<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Actions\Api;


use App\Media\Blog\Domain\ArticleApiFields;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ViewArticleAction extends ArticleAction
{

    public function __invoke(int $id) : JsonResponse
    {
        try{
            $article = $this->repository->viewArticle($id, ArticleApiFields::getFieldsView());
        } catch (EntityNotFoundException $e) {
            return $this->errorMessage('Article not found', Response::HTTP_NOT_FOUND);
        }

        return $this->json($article);
    }

}