<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Forms;


use App\Media\Blog\Domain\ArticleSorting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleListType extends AbstractType
{

    private const DEFAULT_SORT_FIELD = 'id';
    private const DEFAULT_SORT_DEST = 'ASC';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sortField', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'id', 'title'
            ],
            'empty_data' => self::DEFAULT_SORT_FIELD
        ]);
        $builder->add('sortDest', ChoiceType::class, [
            'required' => false,
            'choices' => ['ASC', 'DESC'],
            'empty_data' => self::DEFAULT_SORT_DEST
        ]);
        $builder->add('limit', IntegerType::class, [

        ]);
        $builder->add('offset', IntegerType::class, [

        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArticleSorting::class,
            'csrf_protection' => false,
            'constraints' => []
        ]);
    }
}