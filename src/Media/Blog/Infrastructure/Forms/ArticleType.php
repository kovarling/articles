<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Forms;


use App\Media\Blog\Domain\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArticleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
           'required' => true,
           'constraints' => [
               new NotBlank(),
               new Length([
                   'max' => 255
               ])
           ]
        ]);
        $builder->add('body', TextType::class, [
            'required' => true,
            'constraints' => [
                new NotBlank(),
                new Length([
                    'max' => 255
                ])
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'csrf_protection' => false,
            'constraints' => []
        ]);
    }

}