<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Message;


class DeleteArticleMessage
{

    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}