<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\MessageHandler;

use App\Media\Blog\Domain\ArticleRepository;
use App\Media\Blog\Infrastructure\Message\DeleteArticleMessage;
use App\Media\Blog\Infrastructure\Message\UpdateArticleMessage;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeleteArticleMessageHandler implements MessageHandlerInterface
{

    private ArticleRepository $repository;

    public function __construct(ArticleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DeleteArticleMessage $message
     * @throws EntityNotFoundException
     */
    public function __invoke(DeleteArticleMessage $message)
    {
        $this->repository->delete(
            $message->getId()
        );
    }

}