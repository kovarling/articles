<?php

declare(strict_types=1);


namespace App\Media\Blog\Infrastructure\Repository;


use App\Media\Blog\Domain\Article;
use App\Media\Blog\Domain\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class DoctrineArticleRepository
 * @package App\Media\Blog\Infrastructure\Repository
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctrineArticleRepository implements ArticleRepository
{

    private const ALIAS = 'article';

    private EntityManagerInterface $em;
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Article::class);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $article = $this->findArticle($id);
        $this->em->remove($article);
        $this->em->flush();
    }

    /**
     * @inheritDoc
     */
    public function create(Article $article): Article
    {
        $this->em->persist($article);
        $this->em->flush();
        return $article;
    }

    /**
     * @inheritDoc
     */
    public function update(Article $article): Article
    {
        $article->setUpdatedAt(new \DateTimeImmutable());
        $this->em->flush();
        return $article;
    }


    /**
     * @param $id
     * @return Article
     * @throws EntityNotFoundException
     */
    public function findArticle($id) : Article
    {
        /** @var Article $article */
        $article = $this->repository->find($id);
        if(empty($article)) {
            throw new EntityNotFoundException('Article not found');
        }
        return $article;
    }

    /**
     * @inheritDoc
     */
    public function getList(array $fields, ?string $sortField, ?string $sortDest, ?int $limit, ?int $offset): array
    {
        $qb = $this->repository->createQueryBuilder(self::ALIAS);

        $fields = array_map(function ($item){
            return self::ALIAS.'.'.$item;
        }, $fields);

        $qb->select($fields);

        if(!empty($sortDest) and !empty($sortField)) {
            $qb->orderBy(self::ALIAS.'.'.$sortField, $sortDest);
        }

        if($limit) {
            $qb->setMaxResults($limit);
        }

        if($offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @inheritDoc
     */
    public function viewArticle(int $id, array $fields): array
    {
        $qb = $this->repository->createQueryBuilder(self::ALIAS);

        $fields = array_map(function ($item){
            return self::ALIAS.'.'.$item;
        }, $fields);

        $qb->select($fields);
        $qb->where(self::ALIAS.'.id = :id');
        $qb->setParameter('id', $id);

        $article =  $qb->getQuery()->getSingleResult();

        if (empty($article)) {
            throw new EntityNotFoundException('Article not found');
        }
        return $article;
    }


}