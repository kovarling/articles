<?php


namespace App\Shared\Domain;


interface FieldsFormatter
{
    public static function getFieldsList() : array;
    public static function getFieldsView() : array;
}