<?php

declare(strict_types=1);


namespace App\Shared\Infrastructure;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiAction extends AbstractController
{
    public function successResult(string $message = null): JsonResponse
    {
        return $this->json([
            'success' => true,
            'message' => $message,
            'errors' => [],
        ], Response::HTTP_OK);
    }

    public function errorForm(FormInterface $form): JsonResponse
    {
        return $this->json([
            'success' => false,
            'message' => '',
            'errors' => $form ? $this->getFormErrors($form) : [],
        ], Response::HTTP_BAD_REQUEST);
    }

    public function errorMessage(string $message, $code = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return $this->json([
            'success' => false,
            'message' => $message,
            'errors' => [],
        ], $code);
    }

    protected function getFormErrors(FormInterface $form): array
    {

        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrors($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}