<?php
/**
 * Файл трейта FormJsonTrait
 *
 * @copyright Copyright (c) 2020, Oleg Chulakov Studio
 * @link http://chulakov.com/
 */

namespace App\Shared\Traits;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @mixin AbstractController 
 */
trait FormJsonTrait
{
    private DecoderInterface $decoder;

    /**
     * Create and fill form from json request
     *
     * @param Request $request
     * @param string $type
     * @param object|null $entity
     * @return FormInterface
     */
    protected function formFromJson(Request $request, string $type, ?object $entity = null): FormInterface
    {
        $form = $this->createForm($type);
        if ($entity) {
            $form = $this->createForm($type, $entity);
        }
        return $form->submit($this->decoder->decode($request->getContent(), 'json'));
    }
}
