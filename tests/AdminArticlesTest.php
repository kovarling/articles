<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminArticlesTest extends WebTestCase
{
    private KernelBrowser $client;
    private int $articleId;

    public function testSomething(): void
    {
        $this->client = static::createClient([], [
            'HTTP_X_SECURE_ACCESS' => 'very_secret'
        ]);

        $this->client->xmlHttpRequest('POST', 'http://127.0.0.1:8000/admin/articles/list', [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode(['limit' => 1]));
        $response = $this->client->getResponse();

        $content = json_decode($response->getContent());


        $this->assertResponseIsSuccessful('Article List working');

        $this->testCreate();

        $this->testUpdate();

        $this->testDelete();

    }


    private function testCreate() : void
    {
        $this->client->xmlHttpRequest('POST', 'http://127.0.0.1:8000/admin/articles/', [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'title' => 'test1',
                'body' => 'test1'
            ]));
        $response = $this->client->getResponse();

        $content = json_decode($response->getContent());

        $this->articleId = $content->id;

        $this->assertResponseIsSuccessful('Article Create working');

    }


    private function testUpdate() : void
    {
        $this->client->xmlHttpRequest('POST', 'http://127.0.0.1:8000/admin/articles/'.$this->articleId, [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'title' => 'test1_updated',
                'body' => 'test1_update'
            ]));
        $response = $this->client->getResponse();

        $content = json_decode($response->getContent());
        if($content->title != 'test1_updated' or $content->body != 'test1_update') {
            throw new \Exception('Update failed');
        }
        $this->assertResponseIsSuccessful('Article Update working');

    }


    private function testDelete() : void
    {
        $this->client->xmlHttpRequest('DELETE', 'http://127.0.0.1:8000/admin/articles/'.$this->articleId, [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'title' => 'test1',
                'body' => 'test1'
            ]));

        $this->assertResponseIsSuccessful('Article Delete working');

    }
}
